const { model, Schema } = require('mongoose')

const PetSchema = new Schema({
  name: { type: String, require: true },
  gender: { type: String, require: true },
  race: { type: String, required: true },
  age: { type: String, require: true },
  description: { type: String, require: true },
  img: { type: String, require: true },
},{ versionKey: false })

module.exports = model('pet', PetSchema)