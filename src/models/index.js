module.exports = {
  PetModel: require('./pet'),
  UserModel: require('./user'),
  PublicationModel: require('./publication')
}