const { model, Schema } = require('mongoose')
const { hashSync, compareSync, genSaltSync } = require('bcryptjs')

const UserSchema = new Schema({
  name: { type: String, require: true },
  phone: { type: String, require: true },
  location: { type: String, require: true },
  email: { type: String, require: true },
  password: { type: String, require: true }
},{ versionKey: false })

UserSchema.methods.toJSON = function() {
  let user = this.toObject()
  delete user.password
  return user
}

UserSchema.statics.comparePassword = function(password, receivedPassword) {
  return compareSync(password, receivedPassword)
}

UserSchema.statics.encryptPassword = function(password) {
  const salt = genSaltSync(10)
  return hashSync(password, salt)
}

module.exports = model('user', UserSchema)