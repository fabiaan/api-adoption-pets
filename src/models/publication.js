const { model, Schema } = require('mongoose')

const PublicationSchema = new Schema({
  state: { type: Boolean, default: true },
  pet: { type: Schema.Types.ObjectId, ref: 'pet', required: true },
  user: { type: Schema.Types.ObjectId, ref: 'user', required: true }
},{ versionKey: false })

module.exports = model('publication', PublicationSchema)