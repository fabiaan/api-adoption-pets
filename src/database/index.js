const { connect } = require('mongoose')
const { USER, PASS, DB } = require('../config')

const uri = `mongodb+srv://${USER}:${PASS}@cluster0.tie1l.mongodb.net/${DB}?retryWrites=true&w=majority`
const options = { useNewUrlParser: true, useUnifiedTopology: true }

const connectDB = () => {
  connect(uri, options)
  .then(() => console.log('Connected to MongoDB'))
  .catch(console.error())
}

module.exports = { connectDB }