module.exports = {
  notFound: require('./not-found'),
  verifyToken: require('./verify-token')
}