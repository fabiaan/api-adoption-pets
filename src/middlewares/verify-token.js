const jwt = require('jsonwebtoken')

const { SECRET } = require('../config')
const { UserModel } = require('../models')

module.exports = async (req, res, next) => {
  try {

    const token = req.headers['x-access-token']
    if(!token){
      return res
        .status(403)
        .json({error: 403, message: 'No token provided'})
    }

    const decoded = jwt.verify(token, SECRET)
    req.userId = decoded.id

    const user = await UserModel.findById(req.userId)
    if(!user){
      return res
        .status(404)
        .json({status: 404, message: 'User not found'})
    }

    next()
  } catch (e) {
    return res
      .status(401)
      .json({status: 401, message: 'Unauthorized'})
  }
}
