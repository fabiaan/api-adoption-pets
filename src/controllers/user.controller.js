const { UserModel } = require('../models')

// Find
const getUsers = async (req, res) => {
  try {
    const users = await UserModel.find()
    res.json(users)
  } catch (e) {
    res.json(e)
  }
}

// FindById
const getUserById = async (req, res) => {
  try {
    const { userId } = req.params
    const user = await UserModel.findById(userId)
    res.json(user)
  } catch (e) {
    res.json(e)
  }
}

// Create
const createUser = async (req, res) => {
  try {
    const { body } = req
    const user = await UserModel.create(body)
    res.json(user)
  } catch (e) {
    res.json(e)
  }
}

// Update
const updateUser = async (req, res) => {
  try {
    const { body } = req
    const { userId } = req.params
    const user = await UserModel.findByIdAndUpdate(userId, body, {new:true})
    res.json(user)
  } catch (e) {
    res.json(e)
  }
}

// Delete
const deleteUser = async (req, res) => {
  try {
    const { userId } = req.params
    const user = await UserModel.findByIdAndDelete(userId)
    res.json(user)
  } catch (e) {
    res.json(e)
  }
}

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
}