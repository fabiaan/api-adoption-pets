const { PetModel } = require('../models')

// Find
const getPets = async (req, res) => {
  try {
    const pets = await PetModel.find()
    res.json(pets)
  } catch (e) {
    res.json(e)
  }
}

// FindById
const getPetById = async (req, res) => {
  try {
    const { petId } = req.params
    const pet = await PetModel.findById(petId)
    res.json(pet)
  } catch (e) {
    res.json(e)
  }
}

// Create
const createPet = async (req, res) => {
  try {
    const { body } = req
    const pet = await PetModel.create(body)
    res.json(pet)
  } catch (e) {
    res.json(e)
  }
}

// Update
const updatePet = async (req, res) => {
  try {
    const { body } = req
    const { petId } = req.params
    const pet = await PetModel.findByIdAndUpdate(petId, body, {new:true})
    res.json(pet)
  } catch (e) {
    res.json(e)
  }
}

// Delete
const deletePet = async (req, res) => {
  try {
    const { petId } = req.params
    const pet = await PetModel.findByIdAndDelete(petId)
    res.json(pet)
  } catch (e) {
    res.json(e)
  }
}

module.exports = {
  getPets,
  getPetById,
  createPet,
  updatePet,
  deletePet
}