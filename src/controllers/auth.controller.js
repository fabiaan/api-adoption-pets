const jwt = require('jsonwebtoken')
const validator = require('validator')

const { UserModel } = require('../models')
const { SECRET } = require('../config')
const { validateUser } = require('../helpers')

const signUp = async (req, res) => {
  try {
    const { name, phone, location, email, password } = req.body
    validateUser(name, phone, location, email, password)

    const existsEmail = await UserModel.findOne({email})
    if(existsEmail){
      return res
        .status(400)
        .json({status: 400, message: 'El email ya está registrado'})
    }

    const user = await UserModel.create({
      name, phone, location, email,
      password: await UserModel.encryptPassword(password)
    })

    const token = jwt.sign({id: user._id}, SECRET, {expiresIn: 86400})

    res.json({user, token})
  } catch (e) {
    res
      .status(500)
      .json({status: 500, message: e.message})
  }
}

const signIn = async (req, res) => {
  try {
    const { email, password } = req.body
    const emailValid = validator.isEmail(email)
    if(!emailValid){
      return res
        .status(400)
        .json({status: 400, message: 'El email no es válido'})
    }

    const user = await UserModel.findOne({email})
    if(!user){
      return res
        .status(400)
        .json({status: 400, message: 'Usuario no encontrado'})
    }

    const validPassword = await UserModel.comparePassword(password, user.password)
    if(!validPassword){
      return res
        .status(400)
        .json({status: 400, message: 'Contraseña incorrecta'})
    }

    const token = jwt.sign({id: user._id}, SECRET, {expiresIn: 86400})

    res.json({user, token})
  } catch (e) {
    res
      .status(500)
      .json({status: 500, message: 'Error interno del servidor'})
  }
}

module.exports = {
  signUp,
  signIn
}