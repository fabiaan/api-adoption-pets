const { PublicationModel, PetModel } = require('../models')

// Find
const getPublications = async (req, res) => {
  try {
    const publications = await PublicationModel.find().populate('pet').populate('user')
    res.json(publications)
  } catch (e) {
    res.json(e)
  }
}

// Find filtrando publicaciones activas y que no sean del usuario que esta logueado
const getPublicationsV2 = async (req, res) => {
  try {
    const { userId } = req.params
    const publications = await PublicationModel.find().populate('pet').populate('user')
    const publicationsV2 = publications.filter(publication => {
      return publication.user._id.toString() !== userId.toString() && publication.state === true
    })
    res.json(publicationsV2)
  } catch (e) {
    res.json(e)
  }
}

// FindById
const getPublicationById = async (req, res) => {
  try {
    const { publicationId } = req.params
    const publication = await PublicationModel.findById(publicationId).populate('pet').populate('user')
    res.json(publication)
  } catch (e) {
    res.json(e)
  }
}

// Create
const createPublication = async (req, res) => {
  try {
    const { body } = req
    const publication = await PublicationModel.create(body)
    res.json(publication)
  } catch (e) {
    res.json(e)
  }
}

// Update
const updatePublication = async (req, res) => {
  try {
    const { body } = req
    const { publicationId } = req.params
    const publication = await PublicationModel.findByIdAndUpdate(publicationId, body, {new:true})
    res.json(publication)
  } catch (e) {
    res.json(e)
  }
}

// Delete
const deletePublication = async (req, res) => {
  try {
    const { publicationId } = req.params
    const publication = await PublicationModel.findByIdAndDelete(publicationId)
    await PetModel.findByIdAndDelete(publication.pet)
    res.json(publication)
  } catch (e) {
    res.json(e)
  }
}

// Get publications by userId
const getPublicationsByUserId = async (req, res) => {
  try {
    const { userId } = req.params
    const publications = await PublicationModel.find({user: userId}).populate('pet').populate('user')
    const publicationV2 = publications.filter(publication => {
      return publication.state === true
    })
    res.json(publicationV2)
  } catch (e) {
    res.json(e)
  }
}

module.exports = {
  getPublicationsV2,
  getPublications,
  getPublicationById,
  createPublication,
  updatePublication,
  deletePublication,
  getPublicationsByUserId
}