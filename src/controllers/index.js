module.exports = {
  authController: require('./auth.controller'),
  petController: require('./pet.controller'),
  userController: require('./user.controller'),
  publicationController: require('./publication.controller')
}