module.exports = {
  userRoutes: require('./user.routes'),
  petRoutes: require('./pet.routes'),
  publicationRoutes: require('./publication.routes'),
  authRoutes: require('./auth.routes')
}