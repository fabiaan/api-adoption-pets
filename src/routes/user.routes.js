const { Router } = require('express')
const { userController } = require('../controllers')

const router = Router()

router.get('/', userController.getUsers)
router.get('/:userId', userController.getUserById)
router.post('/', userController.createUser)
router.put('/:userId', userController.updateUser)
router.delete('/:userId', userController.deleteUser)

module.exports = router