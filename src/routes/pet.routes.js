const { Router } = require('express')
const { petController } = require('../controllers')

const router = Router()

router.get('/', petController.getPets)
router.get('/:petId', petController.getPetById)
router.post('/', petController.createPet)
router.put('/:petId', petController.updatePet)
router.delete('/:petId', petController.deletePet)

module.exports = router