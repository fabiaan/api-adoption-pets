const { Router } = require('express')
const { publicationController } = require('../controllers')

const router = Router()

router.get('/:userId', publicationController.getPublicationsByUserId)
router.get('/', publicationController.getPublications)
router.get('/v2/:userId', publicationController.getPublicationsV2)
router.get('/:publicationId', publicationController.getPublicationById)
router.post('/', publicationController.createPublication)
router.put('/:publicationId', publicationController.updatePublication)
router.delete('/:publicationId', publicationController.deletePublication)

module.exports = router