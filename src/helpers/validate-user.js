const validator = require('validator')

module.exports = function(name, phone, location, email, password) {
  try{
    const emailValid = validator.isEmail(email)
    const nameValid = validator.isEmpty(name)
    const phoneValid = validator.isEmpty(phone)
    const locationValid = validator.isEmpty(location)
    const passwordValid = validator.isEmpty(password)

    if(!emailValid) throw new Error('Email is invalid')
    if(nameValid) throw new Error('Name is invalid')
    if(phoneValid) throw new Error('Phone is invalid')
    if(locationValid) throw new Error('Location is invalid')
    if(passwordValid) throw new Error('Password is invalid')
  } catch(err) {
    throw err
  }
}