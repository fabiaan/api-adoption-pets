const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const helmet = require('helmet')
const compression = require('compression')

const { notFound } = require('../middlewares')
const { authRoutes, petRoutes, userRoutes, publicationRoutes } = require('../routes')

let corsOptions = {origin: '*', optionsSuccessStatus: 200}

const app = express()
const version = express.Router()
const api = express.Router()

api
  .use(express.json())
  .use(cors(corsOptions))
  .use(compression())
  .use(helmet())
  .use(morgan('dev'))

api.use('/auth', authRoutes)
api.use('/pet', petRoutes)
api.use('/user', userRoutes)
api.use('/publication', publicationRoutes)
api.use(notFound)

version.use('/v1/api', api)
app.use(version)

module.exports = { app }