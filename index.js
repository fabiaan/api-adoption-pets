const { app } = require('./src/config/app')
const { connectDB } = require('./src/database')

const PORT = process.env.PORT || 3035
connectDB()

app.listen(PORT, () => {
  console.log(`Server running on: http://localhost:${PORT}`)
})